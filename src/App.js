import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <html lang="es">
    <head>
  
        <title>Cabañas Don Juan | Alojamientos en Paso de la Patria</title>
        <meta charset="UTF-8">
        <meta name="description" content="Cabañas en Paso de la Patria Corrientes. Cómodas cabañas totalmente equipadas. Quincho con parrillas, Pileta">
        <meta name="keywords" content="cabañas, paso, corrientes, Paso de la Patria, turismo">
        <link rel="stylesheet" href="style.css">
        <!--[if lt IE 9]><script src="html5ie/dist/html5shiv.js"></script><![endif]-->
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon" />
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <script type="text/javascript" src="jquery.innerfade.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

        <link rel='stylesheet' id='camera-css'  href='Camera-master/css/camera.css' type='text/css' media='all'>
        <script type='text/javascript' src='Camera-master/scripts/jquery.mobile.customized.min.js'></script>
        <script type='text/javascript' src='Camera-master/scripts/jquery.easing.1.3.js'></script> 
        <script type='text/javascript' src='Camera-master/scripts/camera.min.js'></script> 
    </head>      <body>
    	<ul id="bg">
            <li><img src="imagesCabana1.jpg" class="bg"></li>
            <li><img src="imagesCabana2.jpg" class="bg"></li>
            <li><img src="imagesCabana3.jpg" class="bg"></li>
            <li><img src="imagesCabana4.jpg" class="bg"></li>
            <li><img src="imagesCabana5.jpg" class="bg"></li>
        </ul>
        <header>
	<a href="home"><img class="logo" src="imagesCabana5.jpg" /></a>
	<p class="sombra1">
		Cabañas Don Juan y Rincon del Sol/ Corrientes Argentina<br />
		 3644454914   8:30 a 23:59 hs<br />
		<a href="mailto:"gustavoguerraliquidaciones@yahoo.com.ar>gustavoguerraliquidaciones@yahoo.com.ar</a><br />
		<span>Alojamientos en Paso de la Patria</span>
	</p>
	<nav>
		<ul>
			<li><a href="home" class="sel ">Bienvenidos</a></li>
			<li><a href="el-alojamiento" class="">El Alojamiento</a></li>
			<li><a href="saltos-del-mocona" class="">Paso de la Patria</a></li>
			<li><a href="actividades" class="">Actividades</a></li>
			<li><a href="ubicacion" class="">Ubicación</a></li>
			<li><a href="contacto" class="">Contacto</a></li>
		</ul>
	</nav>
</header>        <main id='ma'>
        	<section class='cont home'>
	<h1 class="sombra">Cabañas en Paso de la Patria</h1>
</section>


        </main>
                <footer>
	<div class="foo redes">
		<ul>
			<li><a href="#"><img src="images/twitter.png" alt="Seguinos en Twitter" /></a></li>
			<li><a href="#"><img src="images/youtube.png" alt="Video en Youtube" /></a></li>
			<li><a href="#"><img src="images/facebook.png" alt="Búscanos en Facebook" /></a></li>

		</ul>
	</div>
	<div class="foo nav">
		<ul>
			<li><a href="home">Bienvenidos</a></li>
			<li><a href="el-alojamiento">Alojamiento</a></li>
			<li><a href="Paso de la Patria">Paso de la Patria</a></li>
			<li><a href="actividades">Actividades</a></li>
			<li><a href="ubicacion">Ubicación</a></li>
			<li><a href="contacto">Contacto</a></li>
		</ul>
	</div>
	<div class="foo lang">
		<table cellpadding="0" cellspacing="0">
			<tr>
			</tr>
		</table>
		
		
	</div>
</footer>	   	<script>
            $('#bg').innerfade({
                speed: 1000,
                timeout: 5000,
                type: 'sequence'
                //containerheight: '220px'
            });
            $(document).ready(function(){
               
            });
	   	</script>
    </body>   
</html>
  );
}

export default App;
